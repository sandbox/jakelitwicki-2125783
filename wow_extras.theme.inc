<?php

/**
 *	@author jake.litwicki@gmail.com
 *	@drupal https://drupal.org/user/1878752
 *
 *	@package wow_extras
 *
 *	This module defines extras for the WoW API
 *
 */

function theme_wow_extras_progression($variables) {

	$output = '';
  $character = $variables['character'];
  $uri = wow_character_uri($character);

  $items = array();

  if(isset($character->progression['raids'])) {

  	$raids = $character->progression['raids'];

  	foreach($raids as $raid) {

  		$bosses = $raid['bosses'];
  		$data = array();
  		$heroic = 0;
  		$normal = 0;

  		foreach($bosses as $boss) {

  			$data[$boss['name']]['normal'] = isset($boss['normalKills']) ? $boss['normalKills'] : 0;
  			$data[$boss['name']]['heroic'] = isset($boss['heroicKills']) ? $boss['heroicKills'] : 0;

  			if(isset($boss['heroicKills']) && $boss['heroicKills']) {
  				$heroic++;
  			}

  			if(isset($boss['normalKills']) && $boss['normalKills']) {
  				$normal++;
  			}

  		}

  		$vars = array(
  			'@total' => count($bosses),
  			'@heroic' => $heroic,
  			'@normal' => $normal,
  			'@raid' => $raid['name'],
  		);

  		if($heroic) {
  			$message = t('@heroic/@total Heroic @raid', $vars);
  		}
  		else {
  			$message = t('@normal/@total Normal @raid', $vars);
  		}

  		$items[$raid['name']] = $message;

  	}

  }

  //only return the last raid zone
  $output = '<strong>' . end($items) . '</strong>';

  return $output;

}